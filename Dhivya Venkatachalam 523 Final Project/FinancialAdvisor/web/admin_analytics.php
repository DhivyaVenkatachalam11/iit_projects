<?php
$analytictype="";
if(isset($_POST['submit']))
{
  $analytictype = $_POST["analytictype"];
  
}else{
	  $analytictype = "";
}
?>
<html>
<head>
<title>Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Politics Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap-3.1.1.min.css" rel="stylesheet" type="text/css">
<!-- Custom Theme files -->
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />	
<script src="js/jquery.min.js"> </script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!--/script-->
<link href='//fonts.googleapis.com/css?family=Hammersmith+One' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic,700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body>
	<div class="header" id="home">
		<div class="content white">
			<nav class="navbar navbar-default" role="navigation">
			<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html">Financial Advisor</a>
			</div>
			<!--/.navbar-header-->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				<li><a href="Admin.html">Home</a></li>
						<li><a href="Admin_add.html">ADD Client</a></li>
						<li><a href="admin_advisor.html">ADD Advisor</a></li>
						<li><a href="admin_view.php">View</a></li>
						<li><a href="admin_analytics.php">Analytics</a></li>
						<li><a href="index.php">Logout</a></li>
				</ul>
				
			</div>
			
				<!--/.navbar-collapse-->
				<!--/.navbar-->
			</div>
			</nav>
		</div>
	</div>
   <!--/start-banner-->
	<div class="banner1">
		<div class="container">
		</div>
	</div>
	<!--//end-banner-->
	<!--typography-page -->
	<div class="typo">
		<div class="container">
			<div class="grid_3 grid_4">
				
				<div >
				<form method="post" action=" <?php echo $_SERVER['PHP_SELF'];?> ">
					<table align=left >
					
						<tbody>
						<tr>
													
							<td>
							    <font color=brown face=serif>Analytics:</font>
							
							    <select name='analytictype'>
                                  	<option value='1'>Clients With Maximum Wealth</option>	
									<option value='2'>Count of stock Purchased</option>	
									<option value='3'>Stock Purchased More > 10000 </option>	
									<option value='4'>Maximum Quantities List</option>	
									<option value='5'>Maximum Tax Payers List</option>	
									<option value='6'>Maximum Client Requests </option>
									<option value='7'>Count Of "In Process" Requests (Current Month)</option>	
                                    <option value='8'>Details Of "In Process" Requests (Current Month)</option>										
									
							    </select>
							</td>
													
							
							<td>
									<input type="submit" value="view" name="submit" >
							</td>
						</tr>
				       </table>
				    </form><br/><br/>
					
					<?php
					     // echo date('l, F jS, Y')."<br/><br/><br/>"; 
					     // echo $usertype."<br/>" ;
						  
						      if(!empty($analytictype)){
								  
								
								
								$sql = "SELECT * FROM client";
								
								 echo date('l, F jS, Y')."<br/><br/>"; 
								
								switch($analytictype){
									
									case '1':{
										
										echo "Clients With Maximum Wealth<br/>";
										
										$sql1 = "SELECT asid,clientid,worth FROM assets ORDER BY cast(worth as unsigned) DESC;";
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>Asset_id</th><th>CLient_ID</th><th>Worth</th></tr>";
										
										$tablevalues = array("asid","clientid","worth");
										
										echo " <br/> Asset Data <br/>";
										
										Display($sql1,$tableheader,$tablevalues);
										
										$sql2 = "SELECT asid,clientid,purchaseprice FROM stock ORDER BY cast(PurchasePrice as unsigned) DESC;";
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>Asset_id</th><th>CLient_ID</th><th>PurchasePrice</th></tr>";
										
										$tablevalues = array("asid","clientid","purchaseprice");
										
										echo " <br/><br/>------------------------------------------------------------------------------ <br/><br/>";
										
										echo " <br/>  Stock Data <br/> ";
										
										Display($sql2,$tableheader,$tablevalues);
										
										break;
									}
									case '2':{
										
										echo "Count of stock Purchased<br/>";
										
										$sql1 = "SELECT YEAR(purchasedate) as Year, COUNT(*) as 'count' FROM stock GROUP BY YEAR(purchasedate)" ;
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>Year</th><th>Count</th></tr>";
										
										$tablevalues = array("Year","count");
										
										Display($sql1,$tableheader,$tablevalues);
										
										break;
									}
									case '3':{
										
										echo "Stock Purchased More > 10000 <br/>";
										
										$sql1 = "SELECT * FROM stock WHERE YEAR(purchasedate)=YEAR(CURDATE()) and purchaseprice > 2500;" ;
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>Assetid</th><th>ClientID</th><th>StockName</th><th>Quantity</th><th>purchaseprice</th><th>purchasedate</th></tr>";
										
										$tablevalues = array("asid","clientid","stockname","volume","purchaseprice","purchasedate");
										
										Display($sql1,$tableheader,$tablevalues);
										
										break;
									}
									case '4':{
										
										echo "Maximum Quantities List <br/>";
										
										$sql1 = "SELECT stockname,Volume FROM stock order by cast(volume as unsigned) DESC" ;
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>StockName</th><th>Quantity</th></tr>";
										
										$tablevalues = array("stockname","Volume");
										
										Display($sql1,$tableheader,$tablevalues);
										
										break;
									}
									case '5':{
										
										echo "Maximum Tax Payers List <br/>";
										
										$sql1 = "SELECT ClientID,Propertytype,tDate,Tax FROM tax order by cast(tax.tax as unsigned) DESC" ;
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>ClientID</th><th>Propertytype</th><th>PropertyDate</th><th>Tax</th></tr>";
										
										$tablevalues = array("ClientID","Propertytype","tDate","Tax");
										
										Display($sql1,$tableheader,$tablevalues);
										
										
										break;
									}
									case '6':{
										
										echo "Maximum Client Requests  <br/>";
										
										$sql1 = "SELECT clientid AS c,COUNT(*) AS 'total' FROM clientrequest GROUP BY CAST(clientid AS UNSIGNED) ORDER BY total DESC" ;
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>ClientID</th><th>Total Requests</th></tr>";
										
										$tablevalues = array("c","total");
										
										Display($sql1,$tableheader,$tablevalues);
										
										
										break;
									}
									case '7':{
										
										echo "Count Of In Process Requests (Current Month) <br/>";
										
										$sql1 = "SELECT clientid,MONTH(reqdate) AS Month, COUNT(*) AS 'Count' FROM clientrequest WHERE  MONTH(reqdate)=MONTH(CURDATE()) group by clientid order by Count Desc;" ;
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>clientid</th><th>MONTH</th><th>count</th></tr>";
										
										$tablevalues = array("clientid","Month","Count");
										
										Display($sql1,$tableheader,$tablevalues);
										
										break;
									}
									case '8':{
										
										echo "Details Of In Process Requests (Current Month)  <br/>";
										
										$sql1 = "SELECT Reqid,clientid,Details FROM clientrequest WHERE  status='In Process' and MONTH(reqdate)=MONTH(CURDATE())";
										
										$tableheader = "<Table width=600 border=1 cellspacing=10 > <th>RequestID</th><th>ClientID</th><th>Details</th></tr>";
										
										$tablevalues = array("Reqid","clientid","Details");
										
										Display($sql1,$tableheader,$tablevalues);
										
										break;
									}
									
								}
								
								
								
								
								
								
								

								
								
							  }
							  
							  
							  function Display($query,$th,$tv){
								 $servername = "localhost";
								$username = "root";
								$password = "";
								$dbname = "financial";

								// Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);
								// Check connection
								if ($conn->connect_error) {
									die("Connection failed: " . $conn->connect_error);
								} 
								
								$result = $conn->query($query);
                                 
								if ($result->num_rows > 0) {
									// echo date('l, F jS, Y')."<br/><br/>"; 
									 // echo $usertype.":Data <br/><br/>";
									// output data of each row
									
										echo $th;
										$tvc=count($tv);
										
											while($row = $result->fetch_assoc()) {
												 echo "<tr>";
												   for ($x = 0; $x < $tvc; $x++) {
													   
													   $var = $tv[$x];
													   
													  echo "<td>".$row[$var]."</td>";
													  
												   }
												   echo "</tr>";
											}
											echo "</table>";
								   }else {
									echo "0 results";
								   }
									
								$conn->close();
							  }
							
					  ?>

				
				</div>
			</div>
			
				   
			
		</div>
		
	</div>
			
				


				<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>


<!--JS-->
  <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>

<!--//JS-->

</body>
</html>