/*
SQLyog Community v11.12 Beta1 (32 bit)
MySQL - 5.1.54-community-log : Database - financial
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`financial` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `financial`;

/*Table structure for table `advisor` */

DROP TABLE IF EXISTS `advisor`;

CREATE TABLE `advisor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `contactno` varchar(100) DEFAULT NULL,
  `mailid` varchar(200) DEFAULT NULL,
  `clientid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `advisor` */

insert  into `advisor`(`id`,`name`,`password`,`contactno`,`mailid`,`clientid`) values (1,'VDhivya','VDhivya','9933312342','arjun@gmail.com','1,2,3'),(2,'rahul','rahul','9639639635','rahul@gmail.com','4,5,6'),(3,'lohit','lohit','9996666333','lohit@gmail.com','7,8,9'),(4,'raheem','raheem','8525822285','raheem@gmail.com','10,11'),(5,'raheem','raheem','8525822284','raheem@gmail.com','10,11'),(6,'hussain','hussain','9988556622','hussain@gmail.com','11,12'),(7,'swetha','swetha','9632587411','swetha@gmail.com','12');

/*Table structure for table `assets` */

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `asid` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `details` varchar(500) DEFAULT NULL,
  `worth` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`asid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `assets` */

insert  into `assets`(`asid`,`clientid`,`details`,`worth`) values (1,1,'3BHK apartment','850500'),(2,1,'2 story house ','8740999'),(3,2,'5 indica cabs	  ','2000000'),(4,3,'2BHK apartment','6075000'),(5,7,'shopping Mall  ','25050000');

/*Table structure for table `client` */

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phoneno` varchar(20) DEFAULT NULL,
  `mailid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `client` */

insert  into `client`(`id`,`name`,`password`,`address`,`phoneno`,`mailid`) values (3,'hcl','hcl','electronic city,bangalore','852585225','hcl@gmail.com'),(4,'sourceedge','sourceedge','Kormangala,bangalore','7895852228','se@gmail.com'),(2,'samsung','samsung','kr puram,bangalore','7854785479','sam@gmail.com'),(1,'hp','hp','old airport road,bangalore								  ','7894566563','hp@gmail.com'),(5,'tcs','tcs','Bannerghatta road,bangalore','9856932524','tcs@gmail.com'),(6,'zen','zen','Kormangala,bangalore','9632147865','zen@gmail.com'),(7,'hunet','hunet','Kormangala,bangalore','3222585226','hunet@gmail.com'),(8,'cgi','cgi','Electronic city,bangalore','8525852224','cgi@gmail.com'),(9,'snap','snap','KR Puram ,bangalore	','8759658892','snap@gmail.com'),(11,'vnet','vnet','whitefield,bangalore								  ','7894567861','vnet@gmail.com'),(13,'hp','hp','electronic City,bangalore	','9636963311','arjun@FA.com'),(14,'hcl','hcl','White Field,bangalore	','9636963350','arjun@FA.com'),(15,'cnet','cnet','itpl,bangalore									  ','7896478522','cnet@gmail.com'),(16,'pancake','pancake','MG road,bangalore							  ','7894566333','pancake@gmail.com'),(17,'zeon','zeon','Shivaji Nagar,bangalore		  ','7894525852','zeon@gmail.com'),(18,'domino','domino','HAL,bangalore								  ','852369785','domino');

/*Table structure for table `clientrequest` */

DROP TABLE IF EXISTS `clientrequest`;

CREATE TABLE `clientrequest` (
  `reqid` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` varchar(100) DEFAULT NULL,
  `advisorid` int(10) DEFAULT NULL,
  `details` varchar(500) DEFAULT NULL,
  `reqdate` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`reqid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `clientrequest` */

insert  into `clientrequest`(`reqid`,`clientid`,`advisorid`,`details`,`reqdate`,`status`) values (3,'1',1,' sell all my assets','2015-10-1','In process'),(4,'1',2,' buy assest','2015-12-2','In process'),(5,'1',1,' buy shares from nifty.','2015-12-10','In process'),(6,'2',1,' sale all my shares .','2015-12-11','In process');

/*Table structure for table `fee` */

DROP TABLE IF EXISTS `fee`;

CREATE TABLE `fee` (
  `clientid` int(10) DEFAULT NULL,
  `propertytype` varchar(100) DEFAULT NULL,
  `propertyid` varchar(100) DEFAULT NULL,
  `fdate` varchar(100) DEFAULT NULL,
  `fee` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `fee` */

insert  into `fee`(`clientid`,`propertytype`,`propertyid`,`fdate`,`fee`) values (1,'stock','1','09-12-2015','1200'),(1,'stock','2','09-12-2015','2500'),(1,'stock','1','10-12-2015','100000'),(1,'stock','2','10-12-2015','2500');

/*Table structure for table `stock` */

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `asid` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(10) DEFAULT NULL,
  `stockname` varchar(100) DEFAULT NULL,
  `volume` varchar(100) DEFAULT NULL,
  `purchaseprice` varchar(100) DEFAULT NULL,
  `purchasedate` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`asid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `stock` */

insert  into `stock`(`asid`,`clientid`,`stockname`,`volume`,`purchaseprice`,`purchasedate`) values (1,1,'infosys ','100 ','10000 ','2014-10-20'),(2,1,'Reliance','200 ','25001','2015-10-10'),(3,1,'nifty ','1500 ','15000 ','2015-12-12');

/*Table structure for table `tax` */

DROP TABLE IF EXISTS `tax`;

CREATE TABLE `tax` (
  `clientid` int(10) DEFAULT NULL,
  `propertytype` varchar(100) DEFAULT NULL,
  `propertyid` varchar(100) DEFAULT NULL,
  `tdate` varchar(100) DEFAULT NULL,
  `tax` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tax` */

insert  into `tax`(`clientid`,`propertytype`,`propertyid`,`tdate`,`tax`) values (1,'stock','1','09-12-2015','2500'),(1,'stock','2','09-12-2015','2500'),(1,'stock','1','10-12-2015','2500'),(1,'stock','2','10-12-2015','6250'),(1,'stock','2','10-12-2015','500');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
