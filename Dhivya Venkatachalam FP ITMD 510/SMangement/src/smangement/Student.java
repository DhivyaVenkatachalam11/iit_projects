package smangement;

import java.net.URL;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Author : Dhivya Venkatachalam
 */
public class Student implements Roles, Initializable {

    Stage stage;
    @FXML
    private TextArea clog;
    @FXML
    private TextArea detlog;

    @FXML
    private void handleviewcourses() {
        try {

            Database db = new Database();

            String query = "select * from coursedetails";

            ResultSet rst = db.executeQuery(query);

            StringBuilder Sb = new StringBuilder();
            Sb.append("Course_No " + "\t" + "Prof.Name\n");
            Sb.append("----------------------------------------\n");
            while (rst.next()) {
                Sb.append(rst.getString("coursenumber") + "\t\t" + rst.getString("Profname") + "\n");
            }
            writetoclog(Sb.toString());
        } catch (Exception e) {

            writetoclog("Error in processing request....");
            Common.showdialog("Error", "Error in processing request....");
        }
    }

    @FXML
    private void handleviewdetails(ActionEvent event) {
        try {
            detlog.clear();

            Node source = (Node) event.getSource();
            stage = (Stage) source.getScene().getWindow();
            Database db = new Database();

            String stuid = Common.getinstance().getStudentid();

            String query = "select * from studentdetails where studid=" + stuid.trim() + "";

            ResultSet rst = db.executeQuery(query);

            StringBuilder Sb = new StringBuilder();



            Sb.append("----------------------------------------\n");
            if (rst.next()) {
                Sb.append("ID : " + rst.getString(1) + "\n");
                Sb.append("Name : " + rst.getString(2) + "\n");
                Sb.append("CourseNumber :" + rst.getString(3) + "\n");
                Sb.append("Term : " + rst.getString(4) + "\n");
                Sb.append("FinalAssess : " + rst.getString(5) + "\n");
                Sb.append("CGPA : " + rst.getString(6) + "\n");
                Sb.append("P_F : " + rst.getString(7) + "\n");
                writetostudlog(Sb.toString());

            } else {
                writetostudlog("********* Details Not Available************");
                Common.showdialog("Error", "Details Not Available");
            }



            query = "select * from courseregistration where studid=" + stuid.trim() + "";

            rst = db.executeQuery(query);

            Sb = new StringBuilder();

            Sb.append("CR_ID" + "\t|\t" + "Course Number" + "\t|\t" + "registration_Date\n");
            Sb.append("----------------------------------------\n");
            while (rst.next()) {
                Sb.append(rst.getString(1) + "|");
                Sb.append(rst.getString(3) + "|");
                Sb.append(rst.getString(4) + "|\n");



            }
            writetostudlog(Sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            Common.showdialog("Error", "Error In processing ...");
        }
    }
    @FXML
    private TextField stid, cnum;

    @FXML
    private void handleregistertab(ActionEvent event) {
        System.out.println("Student ID:" + stid);
        stid.setText(Common.getinstance().getStudentid().toString());
    }

    @FXML
    private void handleregistercourse() {
        try {

            // String cid = coid.getText().trim();

            String stuid = stid.getText().trim();;

            String conum = cnum.getText().trim();

            Database db = new Database();

            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

            Calendar cal = Calendar.getInstance();
            String formattedDate = originalFormat.format(cal.getTime());
            System.out.println(formattedDate);

            String query = " insert into courseregistration(StudID,CourseNumber,DateOfregistration) values(" + stuid + ",'" + conum + "','" + formattedDate + "')";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Inserted Rows");
                Common.showdialog("Information", "Inserted Successfully");
            } else {
                System.out.println("Failure");
                Common.showdialog("Information", "Insertion Failure");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    private TextField ucoid, ustid, ucnum;

    @FXML
    private void handleupdatetab(ActionEvent event) {

        String studeid = Common.getinstance().getStudentid().toString();
        ustid.setText(studeid);


    }

    @FXML
    private void handleupdatecourse() {
        try {

            String cid = ucoid.getText().trim();

            String stuid = ustid.getText().trim();;

            String conum = ucnum.getText().trim();

            Database db = new Database();

            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

            Calendar cal = Calendar.getInstance();
            String formattedDate = originalFormat.format(cal.getTime());
            System.out.println(formattedDate);

            String query = " update courseregistration set CourseNumber='" + conum + "' where studid=" + stuid + " and cr_id=" + cid;

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Update Rows");
                Common.showdialog("Information", "Updated Successfully");
            } else {
                Common.showdialog("Information", "Updation Failure");
            }


        } catch (Exception e) {
            e.printStackTrace();
            Common.showdialog("Error", "Error in processing...");
        }
    }

    @FXML
    private void handledeletecourse() {
        try {

            String cid = ucoid.getText().trim();

            String stuid = ustid.getText().trim();;

            String conum = ucnum.getText().trim();

            Database db = new Database();



            String query = " delete from courseregistration where Studid=" + stuid + " and CR_id=" + cid + "";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Deleted Rows");
                Common.showdialog("Information", "Deleted Successfully");
            } else {
                System.out.println("Failure");
                Common.showdialog("Information", "Deletion Failure");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    private TextField fstid, feedback;

    @FXML
    private void handlefeedback(ActionEvent event) {

        String studeid = Common.getinstance().getStudentid().toString();
        fstid.setText(studeid);

    }
    @FXML
    private RadioButton frate1, frate2, frate3, frate4, frate5;

    @FXML
    private void handlefeedbacksubmit() {
        try {

            System.out.println("Handle Feed Back called");
            String stuid = fstid.getText().trim();;

            String content = feedback.getText().trim();

            String Rate = "1";

            if (frate1.isSelected()) {
                Rate = "1";
            } else if (frate2.isSelected()) {
                Rate = "2";
            } else if (frate3.isSelected()) {
                Rate = "3";
            } else if (frate4.isSelected()) {
                Rate = "4";
            } else if (frate5.isSelected()) {
                Rate = "5";
            }


            Database db = new Database();

            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

            Calendar cal = Calendar.getInstance();
            String formattedDate = originalFormat.format(cal.getTime());
            System.out.println(formattedDate);

            String query = "insert into feedback(studid,feedback,fb_date,rate) values (" + stuid + ",'" + content + "','" + formattedDate + "'," + Rate + ")";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Feedback inserted");
                Common.showdialog("Information", "Feedback Successfully");
            } else {
                Common.showdialog("Information", "Feedback Failure");
            }


        } catch (Exception e) {
            e.printStackTrace();
            Common.showdialog("Error", "Error in processing");
        }
    }
    @FXML
    private TextField cstid, complaint;

    @FXML
    private void handlecompbt(ActionEvent event) {

        String studeid = Common.getinstance().getStudentid().toString();
        cstid.setText(studeid);

    }

    @FXML
    private void handlecomplaint() {
        try {



            String stuid = cstid.getText().trim();;

            String content = complaint.getText().trim();


            Database db = new Database();

            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

            Calendar cal = Calendar.getInstance();
            String formattedDate = originalFormat.format(cal.getTime());
            System.out.println(formattedDate);

            String query = "insert into complaints(studid,complaintbox,dateofcomplain,Dateofclosure,resolutiongiven) values (" + stuid + ",'" + content + "','" + formattedDate + "','0000-00-00','')";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Complaint inserted");
                Common.showdialog("Information", "Complaint inserted");
            } else {
                System.out.println("Update Failure");
                Common.showdialog("Information", "Complaint insertion Failure");
            }


        } catch (Exception e) {
            e.printStackTrace();
            Common.showdialog("Error", "Error in processing...");
        }
    }

    /* @FXML
     private void handlelogout() {
     try {

     stage.close();

     Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));

     Scene scene = new Scene(root);
     stage.setTitle("Student Database Management");
     stage.setScene(scene);
     stage.show();
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    @FXML
    private void handlelogout(ActionEvent event) {
        try {
            int ret = Common.showlogoutdialog();

            if (ret == 1) {
                Node source = (Node) event.getSource();

                Stage stage = (Stage) source.getScene().getWindow();

                stage.close();

                Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));

                Scene scene = new Scene(root);
                stage.setTitle("Student Database Management");
                stage.setScene(scene);
                stage.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetoclog(String date) {
        clog.clear();
        clog.setText(date);
    }

    public void writetostudlog(String data) {

        detlog.appendText(data);
    }

    @Override
    public String getUsername() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String ViewStudentDetails() {
        return null;
    }

    public String RegisterComplaints() {
        return null;
    }

    public String GiveFeedBack() {
        return null;
    }

    public String ViewCources() {
        return null;
    }

    public String UpdateCources() {
        return null;
    }

    public String DeleteCources() {
        return null;
    }

    public String Logout() {
        return null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
