package smangement;

import javafx.stage.Stage;

/**
 *
 * @author Dhivya Venkatachalam
 */
public class Common {

    private String username;
    private String userrole;
    private String studentid;
    private Stage stage;

    public static void showdialog(String Title,String msg) {
        int option = 0;
        ChooseStage chooseStage = new ChooseStage(new String[]{"Ok"},
                Title, msg,
                400, 120);
        chooseStage.showAndWait();
        option = chooseStage.getOption();

        /*if (option == 1) {
            System.exit(-1);
        }*/
    }
    
    public static int showlogoutdialog() {
        int option = 0;
        ChooseStage chooseStage = new ChooseStage(new String[]{"Yes","No"},
                "Information", "Are You sure to logout?",
                400, 120);
        chooseStage.showAndWait();
        option = chooseStage.getOption();

        if (option == 1) {
            return 1;
        }
        return 0;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getStudentid() {
        return studentid;
    }

    public String getUsername() {
        return username;
    }

    public String getUserrole() {
        return userrole;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserrole(String userrole) {
        this.userrole = userrole;
    }
    public static Common cinst = null;

    public static Common getinstance() {
        if (cinst == null) {
            cinst = new Common();
        }
        return cinst;
    }
}
