/*
 * 
 * Author : Dhivya Venkatachalam
 */
package smangement;

import java.net.URL;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Author : Dhivya Venkatachalam
 */
public class Complaints implements Initializable {

    @FXML
    private TextField cmid, compid, cddate, rdate;
    @FXML
    private TextArea log;

    @FXML
    private void handleOkAction(ActionEvent event) {

        String comId = cmid.getText().toString();

        setComplaintID(comId);

        status = getStatus();
    }

    @FXML
    private void handleUpdateAction(ActionEvent event) {

        String comId = compid.getText().toString();

        String cldate = cddate.getText().toString();

        String resdate = rdate.getText().toString();

        updateComplaint(comId, cldate, resdate);

    }
    private String complaintID;
    private String status;

    public String getComplaintID() {
        return complaintID;
    }

    public String getStatus() {

        try {
            Database db = new Database();

            String query = " select * from complaints where complaintID=" + getComplaintID() + " ";

            ResultSet rx = db.executeQuery(query);

            if (rx.next()) {
                String data = rx.getString("ResolutionGiven");

                Date cdt = rx.getDate("DateOfClosure");
                log.clear();
                log.setText("Complaint ID:" + getComplaintID() + " was closed on " + cdt.toString() + " \n Resolution Given: " + data);
                
            } else {
                log.clear();
                log.setText("INvalid complaint ID");
                 Common.showdialog("ERROR", "INvalid complaint ID");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.clear();
            log.setText("ERROR");
            Common.showdialog("ERROR", "Error in processing");
        }

        return status;
    }

    @FXML
    private void handlelogout(ActionEvent event) {
        try {

            int ret = Common.showlogoutdialog();

            if (ret == 1) {

                Node source = (Node) event.getSource();

                Stage stage = (Stage) source.getScene().getWindow();

                stage.close();

                Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));

                Scene scene = new Scene(root);
                stage.setTitle("Student Database Management");
                stage.setScene(scene);
                stage.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setComplaintID(String complaintID) {
        this.complaintID = complaintID;
    }

    public void updateComplaint(String cid, String cddate, String rdate) {

        try {
            Database db = new Database();
            System.out.println("Date:" + cddate);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

            Date date = formatter.parse(cddate);

            String pdate = formatter.format(date);

            String query = " update complaints set dateofclosure='" + convertdate(cddate) + "' , resolutiongiven='" + rdate + "' where complaintid=" + cid + " ";

            System.out.println("Query:" + query);
            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Rows Updated");
                Common.showdialog("Information", "Update Success");
            } else {
                System.out.println("Rows not Updated");
                Common.showdialog("Information", "Update Failure");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.clear();
            log.setText("ERROR");
            Common.showdialog("ERROR", "Error in processing request");
        }

    }

    public String convertdate(String cdate) {
        String formattedDate = "";
        try {
            DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = originalFormat.parse(cdate.trim());
            formattedDate = targetFormat.format(date);
            System.out.println(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
