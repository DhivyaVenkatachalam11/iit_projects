//Author : Dhivya Venkatachalam
package smangement;


import java.sql.*;
import java.util.*;


public class Database  {

  	private String  jdbcDriver = "";
  	private String  dbURL = "";
	private String  username = "";
	private String  password = "";

	private Connection connection;


  	public Database() throws SQLException, ClassNotFoundException
  	{

          jdbcDriver  = "com.mysql.jdbc.Driver";
         

         // dbURL = "jdbc:mysql://localhost:3306/studentdatabase?zeroDateTimeBehavior=convertToNull" ;
          dbURL = "jdbc:mysql://www.papademas.net:3306/dbfp";
          username = "fpuser";
          password = "510";
          //username = "root";
          //password = "root";




          Class.forName(jdbcDriver); //set Java database connectivity driver

          connection = DriverManager.getConnection(dbURL, username, password);

	}

	public ResultSet executeQuery(String query)throws SQLException
	{
	    PreparedStatement st  = connection.prepareStatement(query);
	    return st.executeQuery();
	}

	public int executeUpdate(String statement)throws SQLException
	{
	    PreparedStatement st  = connection.prepareStatement(statement);
          
	    return st.executeUpdate();
	}

	public void close()
	{
	    try
	    {
		connection.close();
	    }
	    catch (SQLException sqlException)
	    {
		sqlException.printStackTrace();
	    	connection = null;
	    }
	}
	protected void finalize()
	{
		close();
	}
}

