/*
 * 
 * Author : Dhivya Venkatachalam
 */
package smangement;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 *
 * @Author : Dhivya Venkatachalam 
 */
public class LoginController implements Initializable {

   
    @FXML
    private TextField uname;
    @FXML
    private PasswordField upassword;
    @FXML
    private RadioButton rAdmin, rProfessor, rStudent;
    
    @FXML
    private void handleLoginAction(ActionEvent event) {
        System.out.println("You clicked Login!");

        String username = uname.getText().toString();

        String userpassword = upassword.getText().toString();

        System.out.println("Username:" + username + "  Userpassword:" + userpassword);

        Node source = (Node) event.getSource();
        
        Stage stage = (Stage) source.getScene().getWindow();
        
        Common.getinstance().setStage(stage);

        Validate(username, userpassword);
    }

    public void Validate(String Un, String pwd) {


        System.out.println("Validation Started .....");

        try {

            Database db = new Database();

            String Tablename = "professor";
            if (rAdmin.isSelected()) {
                Tablename = "admin";

                if (Un.equals("admin") && pwd.equals("admin")) {
                    System.out.println("************* Welcome Admin *************  ");
                    Common.getinstance().setUsername(Un);
                    Common.getinstance().setUserrole(Tablename);
                     Common.showdialog("Login Success","Welcome Admin");
                    loadpanel();
                
                  
                }
                return;
            } else if (rProfessor.isSelected()) {
                Tablename = "professor";
            } else if (rStudent.isSelected()) {
                Tablename = "student";
            }

            String query = "select * from " + Tablename + " where Name='" + Un + "' and Password='" + pwd + "'";

            ResultSet rx = db.executeQuery(query);

            if (rx.next()) {
                System.out.println("************* Welcome : " + Un + " *************  ");
                Common.getinstance().setUsername(Un);
                Common.getinstance().setUserrole(Tablename);
                
                 Common.showdialog("Login Success","Welcome "+Un);

                if (Tablename.equals("student")) {
                    Common.getinstance().setStudentid(rx.getString("studid"));
                    loadstudentpanel();
                } else {
                    loadprofessorpanel();
                }

            } else {
                Common.showdialog("Login","Invalid:Login check username and password");
                System.out.println("************* Invalid Login *************  ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadstudentpanel() {
        try {

            Stage stage = Common.getinstance().getStage();
            stage.close();

            Parent root = FXMLLoader.load(getClass().getResource("Studentpanel.fxml"));

            Scene scene = new Scene(root);
            stage.setTitle("Student Database Management :" + Common.getinstance().getUsername());
            
            stage.setScene(scene);
            Common.getinstance().setStage(stage);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadprofessorpanel() {
        try {

           Stage stage = Common.getinstance().getStage();
            stage.close();

            Parent root = FXMLLoader.load(getClass().getResource("Professorpanel.fxml"));

            Scene scene = new Scene(root);
            stage.setTitle("Student Database Management :" + Common.getinstance().getUsername());

            stage.setScene(scene);
            Common.getinstance().setStage(stage);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadpanel() {
        try {

             Stage stage = Common.getinstance().getStage();
            stage.close();

            Parent root = FXMLLoader.load(getClass().getResource("ComplaintsPanel.fxml"));

            Scene scene = new Scene(root);
            stage.setTitle("Student Database Management");
            stage.setScene(scene);
            Common.getinstance().setStage(stage);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
