/*
 * 
 * Author : Dhivya Venkatachalam
 */
package smangement;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @Author : Dhivya Venkatachalam
 */
public class Professor implements Roles, Initializable {

    @FXML
    private TextArea studlog;

    @FXML
    private void handlestudentlog(ActionEvent event) {

        try {
            studlog.clear();
            Database db = new Database();

            String query = "select * from student";

            ResultSet rst = db.executeQuery(query);

            StringBuilder Sb = new StringBuilder();
            Sb.append("Student_Id | Student_name \n");
            Sb.append("-------------------------------------------------------------------------\n\n");
            while (rst.next()) {
                Sb.append(rst.getString(1) + "\t|\t" + rst.getString(2) + "\t|\t" + "\n");
            }
            writetoslog(Sb.toString());

            query = "select * from studentdetails";

            rst = db.executeQuery(query);

            Sb = new StringBuilder();
            Sb.append("\n\n Student_Id | Student_name | Course No | TERM | FinalAssess | CGPA | P_F\n");
            Sb.append("-------------------------------------------------------------------------\n\n");
            while (rst.next()) {
                Sb.append(rst.getString(1) + "\t|\t" + rst.getString(2) + "\t|\t" + rst.getString(3) + "\t|\t"
                        + rst.getString(4) + "\t|\t" + rst.getString(5) + "\t|\t" + rst.getString(6) + "\t|\t"
                        + rst.getString(7) + "\n");
            }
            writetoslog(Sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            writetoslog("Error in processing request....");
            Common.showdialog("ERROR", "Error in processing request....");
        }

    }

    public void writetoslog(String data) {

        studlog.appendText(data);
    }

    @FXML
    private void handlelogout(ActionEvent event) {
        try {
            int ret = Common.showlogoutdialog();

            if (ret == 1) {
                Node source = (Node) event.getSource();

                Stage stage = (Stage) source.getScene().getWindow();

                stage.close();

                Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));

                Scene scene = new Scene(root);
                stage.setTitle("Student Database Management");
                stage.setScene(scene);
                stage.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    private TextArea courselog;

    @FXML
    private void handlecouselog(ActionEvent event) {

        try {

            Database db = new Database();

            String query = "select * from coursedetails where ProfName='" + Common.getinstance().getUsername().trim() + "'";

            ResultSet rst = db.executeQuery(query);

            StringBuilder Sb = new StringBuilder();
            Sb.append("Course No | Professor_Name\n");
            Sb.append("-------------------------------------------------------------------------\n\n");
            while (rst.next()) {
                Sb.append(rst.getString(1) + "\t|\t" + rst.getString(2) + "\n");
            }
            writetocourselog(Sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            writetocourselog("Error in processing request....");
             Common.showdialog("ERROR", "Error in processing request....");
            
        }

    }

    public void writetocourselog(String date) {
        courselog.clear();
        courselog.setText(date);
    }
    @FXML
    private TextArea feedbacklog;

    @FXML
    private void handlefeedbacklog(ActionEvent event) {

        try {

            Database db = new Database();

            String query = "select * from feedback";

            ResultSet rst = db.executeQuery(query);

            StringBuilder Sb = new StringBuilder();
            Sb.append("FB_ID | STU_ID | FEEDBACK | FB_DATE | RATE \n");
            Sb.append("-------------------------------------------------------------------------\n\n");
            while (rst.next()) {
                Sb.append(rst.getString(1) + "\t|\t" + rst.getString(2) + "\t|\t" + rst.getString(3) + "\t|\t"
                        + rst.getString(4) + "\t|\t" + rst.getString(5) + "\t|\t" + "\n");;
            }
            writetofeedbacklog(Sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            writetofeedbacklog("Error in processing request....");
             Common.showdialog("ERROR", "Error in processing request....");
        }

    }

    public void writetofeedbacklog(String date) {
        feedbacklog.clear();
        feedbacklog.setText(date);
    }
    @FXML
    private TextField pstid, pstname, pcn, pterm;

    @FXML
    private void handleaddstudent(ActionEvent event) {
        Database db = null;
        try {

            String stuid = pstid.getText().trim();

            String stuname = pstname.getText().trim();;

            String coursenumber = pcn.getText().trim();

            String term = pterm.getText().trim();

            db = new Database();



            String query = " insert into studentdetails(StudID,StudName,CourseNumber,Term) values(" + stuid + ",'" + stuname + "','" + coursenumber + "','" + term + "')";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                System.out.println("Inserted Rows");
                 Common.showdialog("Information", "Record Inserted successfully");
            } else {
                System.out.println("Failure");
                Common.showdialog("Information", "Record Insertion Failure");
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
             Common.showdialog("ERROR", "Error in processing request....");
        }

    }

    @FXML
    private void handlekeypress(ActionEvent event) {
        try {

            String studeid = pstid.getText().trim();
            Database db = new Database();

            String query = "select * from student where studid='" + studeid + "'";

            ResultSet rst = db.executeQuery(query);


            if (rst.next()) {
                pstname.setText(rst.getString(2));
            } else {
                 Common.showdialog("ERROR", "Invalid ID");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @FXML
    private TextField ustid, ucn, ufa, ucg;

    @FXML
    private void handleupdate(ActionEvent event) {
        Database db = null;
        try {

            String stuid = ustid.getText().trim();

            String finalassess = ufa.getText().trim();

            String CGPA = ucg.getText().trim();

            String coursenumber = ucn.getText().trim();

            String term = pterm.getText().trim();

            db = new Database();

            int tcgpa = Integer.parseInt(CGPA.trim());

            String P_f = "F";
            if (tcgpa > 3) {
                P_f = "P";
            }


            String query = " update studentdetails set finalassess=" + finalassess + ",CGPA=" + CGPA + ",P_F='" + P_f + "' where studid=" + stuid + " and coursenumber='" + coursenumber + "'";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
                Common.showdialog("Information", "Update Successful");
            } else {
                Common.showdialog("Information", "Update Failure");
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void handledelete(ActionEvent event) {
        Database db = null;
        try {

            String stuid = ustid.getText().trim();

            String finalassess = ufa.getText().trim();

            String CGPA = ucg.getText().trim();

            String coursenumber = ucn.getText().trim();

            String term = pterm.getText().trim();

            db = new Database();

            int tcgpa = Integer.parseInt(CGPA.trim());

            String P_f = "fail";
            if (tcgpa > 3) {
                P_f = "PASS";
            }


            String query = " delete from studentdetails where studid=" + stuid + " and coursenumber='" + coursenumber + "'";

            int rows = db.executeUpdate(query);

            if (rows != 0) {
               Common.showdialog("Information", "Delete Successful");
            } else {
               Common.showdialog("Information", "delete Failure");
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void handleupdatekeypress(ActionEvent event) {
        try {

            String studeid = ustid.getText().trim();
            Database db = new Database();

            String query = "select * from studentdetails where studid='" + studeid + "'";

            ResultSet rst = db.executeQuery(query);


            if (rst.next()) {
                ucn.setText(rst.getString(3));
            } else {
                ucn.setText("0");
                Common.showdialog("Error", "Invalid ID");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUsername() {
        return null;
    }

    public String AddStudent() {
        return null;
    }

    public String UpdateStudent() {
        return null;
    }

    public String DeleteStudent() {
        return null;
    }

    public String ViewStudent() {
        return null;
    }

    public String ViewCourSes() {
        return null;
    }

    public String ViewFeedBack() {
        return null;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
