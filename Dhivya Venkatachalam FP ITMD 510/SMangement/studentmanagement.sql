/*
SQLyog Ultimate v8.55 
MySQL - 5.1.54-community : Database - studentdatabase
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`studentdatabase` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `studentdatabase`;

/*Table structure for table `complaints` */

DROP TABLE IF EXISTS `complaints`;

CREATE TABLE `complaints` (
  `ComplaintID` int(11) NOT NULL,
  `StudID` int(11) NOT NULL,
  `ComplaintBox` varchar(500) NOT NULL,
  `DateOfComplain` date NOT NULL,
  `DateOfClosure` date DEFAULT NULL,
  `ResolutionGiven` date DEFAULT NULL,
  PRIMARY KEY (`ComplaintID`),
  KEY `StudID` (`StudID`),
  CONSTRAINT `complaints_ibfk_1` FOREIGN KEY (`StudID`) REFERENCES `studentdetails` (`StudID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `complaints` */

insert  into `complaints`(`ComplaintID`,`StudID`,`ComplaintBox`,`DateOfComplain`,`DateOfClosure`,`ResolutionGiven`) values (1,1,'sdfsadg','2015-12-12','2015-12-25','2015-12-26'),(2,1,'bad teaching','2015-12-05','0000-00-00','0000-00-00'),(3,1,'topic not covered','2015-12-05','0000-00-00','0000-00-00');

/*Table structure for table `coursedetails` */

DROP TABLE IF EXISTS `coursedetails`;

CREATE TABLE `coursedetails` (
  `CourseNumber` varchar(10) NOT NULL,
  `ProfName` varchar(40) NOT NULL,
  PRIMARY KEY (`CourseNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `coursedetails` */

insert  into `coursedetails`(`CourseNumber`,`ProfName`) values ('1','arun'),('2','veeru');

/*Table structure for table `courseregistration` */

DROP TABLE IF EXISTS `courseregistration`;

CREATE TABLE `courseregistration` (
  `CR_ID` int(11) NOT NULL,
  `StudID` int(11) NOT NULL,
  `CourseNumber` varchar(10) NOT NULL,
  `DateOfRegistration` date NOT NULL,
  PRIMARY KEY (`CR_ID`),
  KEY `CourseNumber` (`CourseNumber`),
  KEY `StudID` (`StudID`),
  CONSTRAINT `courseregistration_ibfk_1` FOREIGN KEY (`CourseNumber`) REFERENCES `coursedetails` (`CourseNumber`),
  CONSTRAINT `courseregistration_ibfk_2` FOREIGN KEY (`StudID`) REFERENCES `studentdetails` (`StudID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `courseregistration` */

insert  into `courseregistration`(`CR_ID`,`StudID`,`CourseNumber`,`DateOfRegistration`) values (1,1,'1','0000-00-00'),(3,1,'1','2015-12-05');

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `FB_ID` int(11) NOT NULL,
  `StudID` int(11) NOT NULL,
  `Feedback` varchar(500) NOT NULL,
  `FB_Date` date NOT NULL,
  `Rate` int(11) NOT NULL,
  PRIMARY KEY (`FB_ID`),
  KEY `StudID` (`StudID`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`StudID`) REFERENCES `studentdetails` (`StudID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `feedback` */

insert  into `feedback`(`FB_ID`,`StudID`,`Feedback`,`FB_Date`,`Rate`) values (1,1,'lovely','2015-12-05',5),(2,1,'ok ok not bad','2015-12-05',5);

/*Table structure for table `professor` */

DROP TABLE IF EXISTS `professor`;

CREATE TABLE `professor` (
  `Name` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `professor` */

insert  into `professor`(`Name`,`Password`) values ('arun','arun'),('raghu','raghu');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `studid` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `student` */

insert  into `student`(`studid`,`Name`,`Password`) values ('1','julia','julia'),('2','stacy','stacy'),('3','chandu','chandu'),('4','nazreen','nazreen'),(NULL,'bucky','bucky');

/*Table structure for table `studentdetails` */

DROP TABLE IF EXISTS `studentdetails`;

CREATE TABLE `studentdetails` (
  `StudID` int(11) NOT NULL,
  `Studname` varchar(40) NOT NULL,
  `CourseNumber` varchar(10) NOT NULL,
  `Term` varchar(5) NOT NULL,
  `FinalAssess` int(11) DEFAULT NULL,
  `CGPA` int(11) NOT NULL,
  `P_F` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`StudID`),
  KEY `CourseNumber` (`CourseNumber`),
  CONSTRAINT `studentdetails_ibfk_1` FOREIGN KEY (`CourseNumber`) REFERENCES `coursedetails` (`CourseNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `studentdetails` */

insert  into `studentdetails`(`StudID`,`Studname`,`CourseNumber`,`Term`,`FinalAssess`,`CGPA`,`P_F`) values (1,'julia','1','1',0,5,'5');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
