USE `datamart_application_poc`;

DROP TABLE IF EXISTS dim_applicant;
CREATE TABLE dim_applicant (
 applicant_key INT(12) NOT NULL AUTO_INCREMENT,
 application_id VARCHAR(7) DEFAULT 'Unknown',
 name_prefix VARCHAR(7) DEFAULT '',
 applicant_first_name VARCHAR(32) DEFAULT 'Unknown',
 applicant_middle_name VARCHAR(32) DEFAULT 'Unknown',
 applicant_last_name VARCHAR(32) DEFAULT 'Unknown',
 email_address VARCHAR(32) DEFAULT 'Unknown',
 date_of_birth VARCHAR(10) DEFAULT 'Unknown',
 country_of_citizenship VARCHAR(32) DEFAULT 'Unknown',
 gender VARCHAR(32) DEFAULT 'Unknown',
 admission_decision VARCHAR(32) DEFAULT 'Unknown',
 PRIMARY KEY (applicant_key)
) ENGINE=MYISAM;

DROP TABLE IF EXISTS dim_program;
CREATE TABLE dim_program (
 program_key INT(12) NOT NULL AUTO_INCREMENT,
 program_name VARCHAR(128) NOT NULL DEFAULT '',
 department_name VARCHAR(128) NOT NULL DEFAULT '',   
 PRIMARY KEY (program_key)
) ENGINE=MYISAM;


DROP TABLE IF EXISTS `dim_record`;
CREATE TABLE `dim_record` (
`Recorded_key` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`rec_firstname` VARCHAR(17) DEFAULT NULL,
`rec_lastname` VARCHAR(16) DEFAULT NULL,
`rec_country` VARCHAR(64) CHARACTER SET utf8 DEFAULT NULL,
`rec_lttr_received_dt` VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL) 
ENGINE=MYISAM;


DROP TABLE IF EXISTS dim_applied_college;
CREATE TABLE dim_applied_college
(
`college_id` INT(7) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`application_id` VARCHAR(7) DEFAULT 'Unknown',
`college_name` VARCHAR(128) DEFAULT 'Unknown',
  `college_country` VARCHAR(64) DEFAULT 'Unknown',
  `international` VARCHAR(64) DEFAULT 'Unknown'
 )ENGINE=MYISAM;


DROP TABLE IF EXISTS `dim_address`;
CREATE TABLE `dim_address` (
`address_Key` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
application_id VARCHAR(7) DEFAULT 'Unknown',
`zipcode` VARCHAR(32) CHARACTER SET utf8 DEFAULT 'Unknown',
`city` VARCHAR(32) CHARACTER SET utf8 DEFAULT 'Unknown',
`state_province` VARCHAR(32) CHARACTER SET utf8 DEFAULT 'Unknown',
`province` VARCHAR(32) CHARACTER SET utf8 DEFAULT 'Unknown',
`county` VARCHAR(32) CHARACTER SET utf8 DEFAULT 'Unknown',
`country` VARCHAR(32) CHARACTER SET utf8 DEFAULT 'Unknown',
`address_expires` VARCHAR(11) CHARACTER SET utf8 DEFAULT 'Unknown',
`Type` VARCHAR(11) CHARACTER SET utf8 DEFAULT 'Unknown') 
ENGINE=MYISAM; 

DROP TABLE IF EXISTS dim_ethnicity;
CREATE TABLE dim_ethnicity
( ethnicity_Key INT(7) NOT NULL AUTO_INCREMENT PRIMARY KEY,
application_id VARCHAR(7) DEFAULT 'Unknown',
ethnicity_name VARCHAR(64) CHARACTER SET utf8 DEFAULT NULL
 )ENGINE=MYISAM;

DROP TABLE IF EXISTS fact_application;
CREATE TABLE fact_application (
Fact_key INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 Applicant_key INT(12) DEFAULT 0,
 Program_key INT(12) DEFAULT 0,
 Applied INT(7) NOT NULL DEFAULT 0,
 Admitted INT(7) NOT NULL DEFAULT 0,
 Accepted INT(7) NOT NULL DEFAULT 0,
 Per_address_Key INT NOT NULL DEFAULT 0,
 Cur_address_Key INT NOT NULL DEFAULT 0,
 College_id_1 INT NOT NULL DEFAULT 0,
 College_id_2 INT NOT NULL DEFAULT 0,
 College_id_3 INT NOT NULL DEFAULT 0,
 Recorded_key_1 INT NOT NULL DEFAULT 0,
 Recorded_key_2 INT NOT NULL DEFAULT 0,
 Recorded_key_3 INT NOT NULL DEFAULT 0,
 GRE_Quantitative_Perc INT DEFAULT 0,
 GRE_Analytical_Perc INT DEFAULT 0,
 GRE_Verbal_Perc INT DEFAULT 0,
 FOREIGN KEY FK_APLKY(Applicant_key) REFERENCES dim_applicant(Applicant_key),
 FOREIGN KEY FK_ADDKY1(Per_address_Key) REFERENCES dim_address(address_Key),
  FOREIGN KEY FK_ADDKY2(Cur_address_Key) REFERENCES dim_address(address_Key),
   FOREIGN KEY FK_PGMKY(Program_key) REFERENCES dim_program(Program_key),
    FOREIGN KEY FK_RECKY1(Recorded_key_1) REFERENCES dim_record(Recorded_key),
  FOREIGN KEY FK_RECKY2(Recorded_key_2) REFERENCES dim_record(Recorded_key),
   FOREIGN KEY FK_RECKY3(Recorded_key_3) REFERENCES dim_record(Recorded_key),
    FOREIGN KEY FK_APPCLID1(College_id_1) REFERENCES dim_applied_college(college_id),
  FOREIGN KEY FK_APPCLID2(College_id_2) REFERENCES dim_applied_college(college_id),
   FOREIGN KEY FK_APPCLID3(College_id_3) REFERENCES dim_applied_college(college_id)
)ENGINE=MYISAM;